export const state = () => ({
  alerts: [],
})

export const getters = {
  alerts: (state) => state.alerts,
  hasAlerts: (state) => state.alerts.length > 0,
}

export const actions = {
  store({ commit }, { title, description, job }) {
    commit('pushAlert', { title, description, job })
  },
}

export const mutations = {
  pushAlert(state, payload) {
    state.alerts.push(payload)
  },
}
