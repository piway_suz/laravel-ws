<?php

namespace App\Http\Controllers;

use App\Events\JobCreated;
use App\Models\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function show(Job $job) {
        return $job;
    }

    public function store(Request $request)
    {
        $job = Job::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ]);
        JobCreated::dispatch($job, $request->user());
        return $job;
    }

    public function index()
    {
        return Job::all();
    }

    public function update(Job $job, Request $request)
    {
        $job->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ]);
        return $job;
    }
}
