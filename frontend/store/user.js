export const state = () => ({})

export const getters = {}

export const actions = {
  login({ commit, dispatch }, payload) {
    payload.client_id = process.env.CLIENT_ID
    payload.client_secret = process.env.CLIENT_SECRET
    payload.scope = ''
    payload.grant_type = 'password'

    return new Promise((resolve, reject) => {
      this.$auth
        .loginWith('local', {
          data: payload,
        })
        .then(() => {
          this.$auth.setUser({})
          this.$auth.fetchUser().then((r) => {
            this.$auth.setUser(r.data)
          })
          resolve(true)
        })
        .catch((e) => reject(e))
    })
  },
  logout() {
    this.$auth.logout()
    this.$router.replace('/login')
  },
}

export const mutations = {}
