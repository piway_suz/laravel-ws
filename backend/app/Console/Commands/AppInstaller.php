<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Passport;

class AppInstaller extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'App setup script';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call("cache:clear", [], $this->output);
        Artisan::call("config:clear", [], $this->output);
        Artisan::call("migrate", [], $this->output);

        $id = env('PASSPORT_CLIENT_ID');
        $secret = env('PASSPORT_CLIENT_SECRET');

        $client = Passport::client()->forceFill([
            'id' => $id,
            'user_id' => null,
            'name' => 'Dev Passport Client',
            'secret' => $secret,
            'provider' => 'users',
            'redirect' => config('app.url'),
            'personal_access_client' => false,
            'password_client' => true,
            'revoked' => false,
        ]);
        $client->save();
        $this->info("Added Passport Client $id:$secret");

        Artisan::call("passport:keys", [], $this->output);
        Artisan::call("db:seed", [], $this->output);
        return 0;
    }
}
