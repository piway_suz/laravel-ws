export const state = () => ({
  jobs: [],
})

export const getters = {
  jobs: (state) => state.jobs,
  byId: (state) => (id) => state.jobs.find((job) => job.id === parseInt(id)),
}

export const actions = {
  index({ commit }) {
    return this.$axios.get('/api/jobs').then((response) => {
      commit('setJobs', response.data)
    })
  },
  store({ commit }, { title, description }) {
    return this.$axios
      .post('/api/jobs', { title, description })
      .then((response) => {
        commit('pushJob', response.data)
      })
  },
  show({ commit, state }, id) {
    return this.$axios.get(`/api/jobs/${id}`).then((response) => {
      const index = state.jobs.findIndex((job) => job.id === id)

      if (index < 0) {
        commit('pushJob', response.data)
      } else {
        commit('replaceJob', { job: response.data, index })
      }
    })
  },
  update({ commit, state }, { id, title, description }) {
    return this.$axios
      .patch(`/api/jobs/${id}`, { title, description })
      .then((response) => {
        const index = state.jobs.findIndex((job) => job.id === id)
        if (index < 0) {
          commit('pushJob', response.data)
        } else {
          commit('replaceJob', { job: response.data, index })
        }
      })
  },
}

export const mutations = {
  setJobs(state, payload) {
    state.jobs = payload
  },
  pushJob(state, payload) {
    state.jobs.push(payload)
  },
  replaceJob(state, payload) {
    state.jobs.push(payload)
  },
}
